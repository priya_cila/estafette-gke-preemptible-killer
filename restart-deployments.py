from kubernetes import client, config
from kubernetes.client.rest import ApiException
import datetime
import argparse
import logging
import time 

logging.basicConfig(
    format="%(asctime)s %(levelname)s: %(message)s",
    level=logging.INFO,
)
log = logging.getLogger(__name__)

# Initialize parser
parser = argparse.ArgumentParser()
 
# Adding optional argument
parser.add_argument("-n", "--node", help = "Kubernetes node for rolling restart")
 
# Read arguments from command line
args = parser.parse_args()

def restart_deployment(v1_apps, deployment, namespace):
    now = datetime.datetime.utcnow()
    now = str(now.isoformat("T") + "Z")
    body = {
        'spec': {
            'template':{
                'metadata': {
                    'annotations': {
                        'kubectl.kubernetes.io/restartedAt': now
                    }
                }
            }
        }
    }
    try:
        v1_apps.patch_namespaced_deployment(deployment, namespace, body, pretty='true')
    except ApiException as e:
        if 'not found' in str(e):
            log.warning("Exception when trying to restart deployment.  Error: {} not found. This can be ignored".format(deployment))
        else:
            log.warning("Exception when calling AppsV1Api->read_namespaced_deployment_status: %s\n" % e)

def wait_for_deployment_complete(api, deployment_name, namespace, timeout=60):
    start = time.time()
    try:
        while time.time() - start < timeout:
            time.sleep(10)
            response = api.read_namespaced_deployment_status(deployment_name, namespace)
            s = response.status
            if (s.updated_replicas == response.spec.replicas and
                    s.replicas == response.spec.replicas and
                    s.available_replicas == response.spec.replicas and
                    s.observed_generation >= response.metadata.generation):
                return True
            else:
                log.info ("Waiting for deployment {} to be completed ...".format(deployment_name))
                # log.info(f'[updated_replicas:{s.updated_replicas},replicas:{s.replicas}'
                #     ',available_replicas:{s.available_replicas},observed_generation:{s.observed_generation}] waiting...')
        raise RuntimeError(f'Waiting timeout for deployment {deployment_name}')
    except:
        log.warning("Unable to get deployment status {}".format(deployment_name))

def main():
    
    if args.node:
        log.info("Performing rolling restart on deployments from node {}".format(args.node))
        
        # Configs can be set in Configuration class directly or using helper utility
        config.load_incluster_config()
        
        v1 = client.CoreV1Api()
        v1_apps = client.AppsV1Api()

        ret = v1.list_pod_for_all_namespaces(watch=False)

        pods=[]
        # deployments = set()
        deployments={}

        for i in ret.items:
            if(i.metadata.namespace != "estafette"):
                if (i.spec.node_name == args.node):
                    pods.append(i.metadata.name)
                    deployment_name='-'.join(i.metadata.name.split("-")[0:-2])
                    if (i.metadata.namespace) not in deployments.keys(): 
                        deployments[i.metadata.namespace]=set([deployment_name])
                    else:
                        deployments[i.metadata.namespace].add(deployment_name)

        log.info("Deployments to be restarted -  {}".format(deployments))

        for ns,ds in deployments.items():
            for d in ds:
                log.info("Restarting deployment {}".format(d))
                restart_deployment(v1_apps, d, ns)
                wait_for_deployment_complete(v1_apps, d, ns)
                log.info("Restart of deployment {} is completed".format(d))
        log.info("Rolling restart of deployments in node {} is completed".format(args.node))

if __name__ == "__main__":
    main()



    
