FROM python

LABEL maintainer="estafette.io" \
      description="The estafette-gke-preemptible-killer component is a Kubernetes controller that ensures preemptible nodes in a Container Engine cluster don't expire at the same time"

RUN pip install kubernetes

#COPY ca-certificates.crt /etc/ssl/certs/
COPY estafette-gke-preemptible-killer /
COPY restart-deployments.py /

CMD ["./estafette-gke-preemptible-killer"]
